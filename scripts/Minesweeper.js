class Minesweeper {
    header = null;
    mainBoard = null;
    autoSolver = null;
    placeFlag = false;
    gameState = "playing";
    constructor(x, y, bombs) {
        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height() - 50;
        if (viewportHeight > viewportWidth) {
            this.header = new Header(bombs);
            this.mainBoard = new Board(x, y, bombs);
        } else {
            this.header = new Header(bombs);
            this.mainBoard = new Board(x, y, bombs);
        }
        this.OnResize();
    }

    GetBoard() {
        return this.mainBoard.GetBoard();
    }

    GetFlagCount() {
        return this.header.flagCount;
    }

    SquareClickInput(item) {
        if (this.gameState == "playing") {
            var i = this.mainBoard.OnClickInput(item, this.placeFlag);
            if (i == 2) {
                this.gameState = "end";
            } else if (i == 1 || i == -1) {
                this.header.UpdateFlagCount(i);
            }
        }
        if (this.mainBoard.CheckVictory()) {
            this.gameState = "end";
            this.End();
        }
    }

    FlagButtonClick(item) {
        this.placeFlag = this.header.OnButtonClick(item, this.placeFlag);
        if (this.mainBoard.CheckVictory()) {
            this.End();
        }
    }

    ResetButtonClick(x, y, b) {
        this.mainBoard.ResetAll(x, y, b);
        this.OnResize();
        this.header.SetFlagCount(b);
        this.gameState = "playing";
    }

    OnResize() {
        var viewportHeight = $(window).height() - 50;
        var viewportWidth = document.getElementById("contain").offsetWidth
        if (viewportHeight > viewportWidth) {
            this.mainBoard.OnResize(viewportWidth, viewportWidth);
            this.header.OnResize(viewportWidth, viewportWidth);
        } else {
            this.mainBoard.OnResize(viewportHeight, viewportHeight);
            this.header.OnResize(viewportHeight, viewportHeight);
        }
    }

    End() {
        this.mainBoard.SquareDance();
    }
}