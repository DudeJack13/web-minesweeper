class Board {
    xSize = 0;
    ySize = 0;
    width = 0;
    height = 0;
    board = [];
    bombs = [];
    bombsAmount = 0;
    squaresLeft = 0;
    colors = {
        _1: "#0100f6",
        _2: "#017b01",
        _3: "#f60000",
        _4: "#01007c",
        _5: "#7d0102",
        _6: "#007c7d",
        _7: "#000000",
        _8: "#7b7b7b"
    };

    constructor(xSize, ySize, bombs) {
        this.xSize = xSize;
        this.ySize = ySize;
        this.bombsAmount = bombs;
        this.squaresLeft = this.xSize * this.ySize;
        this._CreateBoard();
        this._GenerateBombs(bombs);
    }

    GetBoard() {
        var returnVal = [];
        this.board.forEach(b => {
            var number = -1;
            if (b.visible) {
                number = b.number
            }
            var r = [{
                id: b.id,
                x: b.x,
                y: b.y,
                flagged: b.flagged,
                number: number,
                visible: b.visible
            }];
            returnVal.push(r);
        });
        return returnVal;
    }

    ResetAll(xSize, ySize, bombAmount) {
        this.board.forEach(b => {
            let div = document.getElementById(b.id);
            div.parentNode.removeChild(div);
        });
        let main = document.getElementById('main');
        main.parentNode.removeChild(main);
        this.board.splice(0, this.board.length);
        this.bombs.splice(0, this.bombs.length);
        this.xSize = xSize;
        this.ySize = ySize;
        this.bombsAmount = bombAmount;
        this.squaresLeft = this.xSize * this.ySize;
        this._CreateBoard();
        this._GenerateBombs(this.bombsAmount);
    }

    _CreateBoard() {
        var mainEl = document.getElementById('bC');
        //Create table html
        var top = document.createElement('div');
        top.classList.add("class", "grid-container")
        top.setAttribute("id", "main");
        mainEl.appendChild(top);

        //Create table css

        var tableHeight = "";
        var tableWidth = "";
        for (let i = 0; i < this.xSize; i++) {
            tableHeight += String(this.width / this.xSize) + "px ";
            //a += "auto "
        }
        for (let i = 0; i < this.ySize; i++) {
            tableWidth += String(this.height / this.ySize) + "px ";
            //a += "auto "
        }
        $('.grid-container').css({
            'grid-template-columns': tableWidth,
            'grid-template-rows': tableHeight,
            'height': String(this.height),
            'width': String(this.width),
            'display': 'grid',
        });


        //Create grid squares
        var counterX = 0;
        var counterY = 0;
        for (let i = 0; i < this.xSize * this.ySize; i++) {
            var div = document.createElement("div");
            div.classList.add("square");
            div.classList.add("hidden");
            var id = String(i);
            div.setAttribute("id", id);
            div.setAttribute("onClick", "clicked(this);");
            top.appendChild(div);
            this.board.push({
                id: i,
                x: counterX,
                y: counterY,
                number: 0,
                visible: false,
                bomb: false,
                flagged: false
            });
            counterX++;
            if (counterX >= this.xSize) {
                counterX = 0;
                counterY++;
            }
        }
        console.log(this.board);
    };

    OnResize(height, width) {
        this.height = height;
        this.width = width;
        var tableHeight = "";
        var tableWidth = "";
        for (let i = 0; i < this.xSize; i++) {
            tableHeight += String(this.width / this.xSize) + "px ";
            //a += "auto "
        }
        for (let i = 0; i < this.ySize; i++) {
            tableWidth += String(this.height / this.ySize) + "px ";
            //a += "auto "
        }
        $('.grid-container').css({
            'grid-template-columns': tableWidth,
            'grid-template-rows': tableHeight,
            'height': String(this.height),
            'width': String(this.width),
            'display': 'grid',
        });
        //Make the squares square
        $('.square').css({
            'font-size': String($('.square').width()) / 1.5 + "px"
        });
    }

    _GenerateBombs(bombs) {
        var bombList = [];
        for (let i = 0; i < bombs; i++) {
            var rnums = null;
            do {
                rnums = this._GenerateBombNumbers();
            } while (this._CheckRepeat(bombList, rnums));
            bombList.push(rnums);
        }

        bombList.forEach(b => {
            var s = this.GetSquareFromCoords(b[0], b[1]);
            this.board[s[1]].bomb = true;
            this.board[s[1]].number = -1;
            this.bombs.push(s);
        });
        this.bombs.forEach(b => {
            this._GenerateSurroundingNumbers(b);
        });
    }

    _CheckRepeat(bombList, nums) {
        for (let b = 0; b < bombList.length; b++) {
            if (bombList[b][0] == nums[0] && bombList[b][1] == nums[1]) {
                return true;
            }
        }
        return false;
    }

    _GenerateBombNumbers() {
        return [Math.floor(Math.random() * this.xSize), Math.floor(Math.random() * this.ySize)];
    }

    _GenerateSurroundingNumbers(squareInfo) {
        //left
        var surrounding = this._GetSurroundingSquares(this.board[squareInfo[1]].id);
        if (surrounding.left != undefined && surrounding.left != null) {
            this._UpdateCell(surrounding.left);
        }
        //right
        if (surrounding.right != undefined && surrounding.right != null) {
            this._UpdateCell(surrounding.right);
        }
        //top
        surrounding.top.forEach(i => {
            if (i != undefined && i != null) {
                this._UpdateCell(i);
            }
        });
        //bottom 
        surrounding.bottom.forEach(i => {
            if (i != undefined && i != null) {
                this._UpdateCell(i);
            }
        });
    }

    _GetSurroundingSquares(id) {
        var returnVal = {
            left: null,
            right: null,
            top: null,
            bottom: null,
            all: null
        };
        var all = []
        if (this.board[this.board[id].id - 1] != undefined) {
            if (this.board[this.board[id].id - 1].y == this.board[id].y) {
                returnVal.left = this.board[this.board[id].id - 1];
                all.push(this.board[this.board[id].id - 1]);
            }
        } else {
            returnVal.left = undefined;
            all.push(undefined);
        }
        if (this.board[this.board[id].id + 1] != undefined) {
            if (this.board[this.board[id].id + 1].y == this.board[id].y) {
                returnVal.right = this.board[this.board[id].id + 1];
                all.push(this.board[this.board[id].id + 1]);
            }
        } else {
            returnVal.right = undefined;
            all.push(undefined);
        }

        var top = [];
        for (let s = this.board[id].id - (this.xSize + 1); s <= this.board[id].id - (this.xSize - 1); s++) {
            if (s >= 0) {
                if (this.board[s].y == this.board[id].y - 1) {
                    top.push(this.board[s]);
                    all.push(this.board[s]);
                }
            }
        }
        returnVal.top = top;
        var bottom = [];
        for (let s = this.board[id].id + (this.xSize - 1); s <= this.board[id].id + (this.xSize + 1); s++) {
            if (s < this.board.length) {
                if (this.board[s].y == this.board[id].y + 1) {
                    bottom.push(this.board[s]);
                    all.push(this.board[s]);
                }
            }
        }
        returnVal.bottom = bottom;
        returnVal.all = all;
        return returnVal;
    }

    _UpdateCell(obj) {
        if (!this.board[obj.id].bomb) {
            this.board[obj.id].number += 1;
        }
    }

    _UpdateTextColour(div, number) {
        if (number == 1) {
            div.style.color = this.colors._1;
        } else if (number == 2) {
            div.style.color = this.colors._2;
        } else if (number == 3) {
            div.style.color = this.colors._3;
        } else if (number == 4) {
            div.style.color = this.colors._4;
        } else if (number == 5) {
            div.style.color = this.colors._5;
        } else if (number == 6) {
            div.style.color = this.colors._6;
        } else if (number == 7) {
            div.style.color = this.colors._9;
        } else if (number == 8) {
            div.style.color = this.colors._8;
        }
    }

    GetSquareFromID(id) {
        for (let b = 0; b < this.board.length; b++) {
            if (this.board[b].id == id) {
                return [this.board[b], b];
            }
        }
    }

    GetSquareFromCoords(x, y) {
        for (let b = 0; b < this.board.length; b++) {
            if (this.board[b].x == x && this.board[b].y == y) {
                return [this.board[b], b];
            }
        }
    }

    OnClickInput(item, placeFlag) {
        var square = this.GetSquareFromID($(item).attr("id"));
        if (!placeFlag) {
            if (square[0].bomb) {
                this._UnhideAllBombs();
                return 2;
            } else {
                this._UnhideSquares(square);
            }
            return 3;
        } else if (placeFlag && !square[0].visible) {
            var div = document.getElementById(square[0].id);
            if (!square[0].flagged) {
                this.board[square[0].id].flagged = true;
                div.classList.remove("hidden");
                div.classList.add("flag");
                div.innerHTML = "<i class='fas fa-flag'></i>";
                this.squaresLeft--;
                return -1;
            } else {
                this.board[square[0].id].flagged = false;
                div.classList.remove("flag");
                div.classList.add("hidden");
                div.innerHTML = "";
                this.squaresLeft++;
                return 1;
            }
        } else {
            return 0;
        }
    }

    _UnhideAllBombs() {
        this.bombs.forEach(b => {
            var div = document.getElementById(b[0].id);
            div.classList.remove("hidden");
            div.classList.remove("flag");
            div.classList.add("bomb");
            if (!b[0].flagged) {
                div.innerHTML = "<i class='fas fa-bomb'></i>";
            }
        });
    }

    _UnhideSquares(square) {
        var toCheck = [square];
        do {
            var s = toCheck.shift();
            if (!this.board[s[0].id].visible && !this.board[s[0].id].flagged) {
                this.board[s[0].id].visible = true;
                this.squaresLeft--;
                var div = document.getElementById(s[0].id);
                div.classList.remove("hidden");
                if (this.board[s[0].id].number > 0) {
                    var div = document.getElementById(s[0].id);
                    div.innerText = String(this.board[s[0].id].number);
                    this._UpdateTextColour(div, this.board[s[0].id].number);
                }
                if (s[0].number == 0) {
                    var around = this._GetSurroundingSquares(s[0].id);
                    around.all.forEach(a => {
                        if (a != undefined) {
                            toCheck.push(this.GetSquareFromID(a.id));
                        }
                    });
                }
            }
        } while (toCheck.length > 0);
    }

    CheckVictory() {
        return this.squaresLeft == 0;
    }

    SquareDance() {
        this.board.forEach(b => {
            if (!b.bomb) {
                var div = document.getElementById(b.id);
                div.classList.add("animate");
            }
        });
    }
};