class Header {
    flagCount = 0;
    flagButton = null;
    h1Div = null;
    constructor(bombCount) {
        this.flagCount = bombCount;
        this._CreateHeader();
    };

    _CreateHeader() {
        let mainEl = document.getElementById('bC');
        let div = document.createElement("div");
        div.setAttribute("id", "header");
        mainEl.appendChild(div);
        div.classList.add("headerBar");
        $(".headerBar").css({
            'height': '50px',
            'display': 'grid',
            'grid-template-columns': '25% 50% 25%',
            'grid-template-rows': '100%',
            'color': 'white',
            'text-align': 'center',
        });

        this.h1Div = document.createElement('div');
        this.h1Div.classList.add("hDiv");

        let flagButton = document.createElement("button");
        flagButton.classList.add("hButton");
        flagButton.setAttribute("id", "hButtonID");
        flagButton.setAttribute("onClick", "flagClicked(this);");

        let buttonSet = document.createElement("div");
        buttonSet.classList.add("buttonSet");

        let resetButton = document.createElement("button");
        resetButton.classList.add("hButton1");
        resetButton.setAttribute("onClick", "resetClicked(this);");
        resetButton.setAttribute("title", "Reset");

        let autoButton = document.createElement("button");
        autoButton.classList.add("hButton1");
        autoButton.setAttribute("onClick", "_Loop();");;
        autoButton.setAttribute("title", "Auto Solve");;

        let settingsButton = document.createElement("button");
        settingsButton.classList.add("hButton1");
        settingsButton.setAttribute("onClick", "settingsClicked();");
        settingsButton.setAttribute("title", "Settings");
        settingsButton.setAttribute("id", "dropdownSettingsButton");

        buttonSet.appendChild(resetButton);
        buttonSet.appendChild(autoButton);
        buttonSet.appendChild(settingsButton);

        div.appendChild(this.h1Div);
        div.appendChild(flagButton);
        div.appendChild(buttonSet);

        this._Settings(div);

        this.h1Div.innerHTML = "<i class='fas fa-flag'></i> " + String(this.flagCount);
        flagButton.innerHTML = "<i class='fas fa-flag'></i>";
        resetButton.innerHTML = "<i class='fas fa-redo'></i>";
        autoButton.innerHTML = "<i class='fas fa-laptop-code'></i>";
        settingsButton.innerHTML = "<i class='fas fa-cog'></i>";
        $(".hButton").css({
            'margin': 'auto',
            'padding': '5px 10px 5px 10px',
        });
        $(".hButton1").css({
            'margin-left': '5px',
            'padding': '5px 10px 5px 10px',
        });
        $(".hDiv").css({
            'margin': 'auto',
        });
        $(".dropdown-content").css({
            'padding': '5px',
        });
        $(".pButton").css({
            'margin-right': '10px',
        });
        $(".buttonSet").css({
            'width': '145px',
            'grid-template-columns': 'auto auto auto',
            'grid-template-rows': '100%',
            'margin': 'auto',
        });
    }

    _Settings(div) {
        let settingBox = document.createElement("div");
        settingBox.classList.add("dropdown-content");
        settingBox.setAttribute("id", "settingsBox");

        let sliderBoaderText = document.createElement("label");
        sliderBoaderText.setAttribute("id", "sliderBoardText");
        sliderBoaderText.innerText = "temp1";

        let sliderBoard = document.createElement("input");
        sliderBoard.classList.add("form-control-range");
        sliderBoard.setAttribute("id", "sliderBoard");
        sliderBoard.setAttribute("type", "range");
        sliderBoard.setAttribute("min", "2");
        sliderBoard.setAttribute("max", "50");
        sliderBoard.setAttribute("value", "20");
        sliderBoard.setAttribute("onchange", "UpdateSliderBoard()");
        sliderBoaderText.innerText = "Border Size: " + String(sliderBoard.value);

        let sliderBombText = document.createElement("label");
        sliderBombText.setAttribute("id", "sliderBombText");
        sliderBombText.innerText = "temp2";

        let sliderBomb = document.createElement("input");
        sliderBomb.classList.add("form-control-range");
        sliderBomb.setAttribute("id", "sliderBomb");
        sliderBomb.setAttribute("type", "range");
        sliderBomb.setAttribute("min", "1");
        sliderBomb.setAttribute("max", "200");
        sliderBomb.setAttribute("value", "30");
        sliderBomb.setAttribute("onchange", "UpdateSliderBomb()");
        sliderBombText.innerText = "Bombs: " + String(sliderBomb.value);

        let resetButton = document.createElement("button");
        resetButton.setAttribute("onClick", "resetClicked(document.getElementById('hButtonID'))");
        resetButton.classList.add("resetButton");
        resetButton.innerHTML = "<i class='fas fa-wrench'></i>  Generate";

        settingBox.appendChild(sliderBoaderText);
        settingBox.appendChild(sliderBoard);
        settingBox.appendChild(sliderBombText);
        settingBox.appendChild(sliderBomb);
        settingBox.appendChild(resetButton);
        div.appendChild(settingBox);
    }

    OnResize(height, width) {
        $(".headerBar").css({
            'width': String(width),
        });
        $('.dropdown-content').css({
            'margin-top': "55px",
            'width': String(width - 20),
            'margin-left': '10px',
        });
    }

    OnButtonClick(item, placeFlag) {
        if (placeFlag) {
            item.innerHTML = "<i class='fas fa-flag'></i>";
            return false;
        } else if (!placeFlag) {
            item.innerHTML = "<i class='fas fa-bomb'></i>";
            return true;
        }
    }

    UpdateFlagCount(i) {
        this.flagCount += i;
        this.h1Div.innerHTML = "<i class='fas fa-flag'></i> " + String(this.flagCount);
    }

    SetFlagCount(i) {
        this.flagCount = i;
        this.h1Div.innerHTML = "<i class='fas fa-flag'></i> " + String(this.flagCount);
    }
}