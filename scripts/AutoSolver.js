class AutoSolver {
    moveCounter = 0;
    moveBuffer = [];

    commands = {
        click: "click",
        flag: "flag",
    };

    //---------------------------------------------
    //Auto solver
    //---------------------------------------------

    GetMove(board) {
        this.moveCounter++;
        console.log("Move: " + String(this.moveCounter));
        var commands = [];
        //Check for all moves on the board
        for (let bL = 0; bL < board.length; bL++) {
            var s = board[bL][0];
            if (s.number > 0) {
                var sur = this._GetSurrording(s.id, board)[0];

                //If flags surrounding matches number
                var state = this.commands.click;
                if (s.number == (sur.hidden + sur.flagged)) {
                    state = this.commands.flag;
                }
                var riskFactor = this._GetRiskFactor(s, sur);
                for (let s2 = 0; s2 < sur.all.length; s2++) {
                    var s1 = sur.all[s2][0];
                    if (state == this.commands.flag && !s1.visible && !s1.flagged) {
                        commands.push(this._CreateCommand(this.commands.flag, s1.id, riskFactor, "Flags number matches square number"));
                    } else if (state == this.commands.click && !s1.visible && !s1.flagged) {
                        commands.push(this._CreateCommand(this.commands.click, s1.id, riskFactor, "Flags number matches square number"));
                    }
                }
            }
        }
        //Catch all, no rules match, choose at randon
        if (commands.length == 0) {
            var randomId = Math.floor(Math.random() * board.length - 1 + 1);
            return this._CreateCommand(this.commands.click, randomId, 0, "Random");
        }

        //Return best command
        var bestCommand = this._CreateCommand(null, null, 100, null);
        for (let c = 0; c < commands.length; c++) {
            if (commands[c].riskFactor < bestCommand.riskFactor) {
                bestCommand = commands[c];
            }
        }
        return bestCommand;
    }

    _GetRiskFactor(s, sur) {
        return Math.abs((s.number + sur.hidden) - sur.flagged);
    }

    _CreateCommand(command, id, riskFactor, reason) {
        return {
            command: command,
            id: id,
            riskFactor: riskFactor,
            reason: reason,
        };
    }

    //---------------------------------------------
    //Sqaure finder
    //---------------------------------------------

    _GetSurrording(id, board) {
        //Left
        var hidden = 0;
        var flagged = 0;
        var amount = 0;
        var xLength = board[board.length - 1][0].x + 1;
        var left = null;
        var all = []
        if (id - 1 >= 0) {
            if (board[id - 1][0].y == board[id][0].y) {
                left = board[id - 1];
                all.push(board[id - 1]);
                if (left[0].flagged) {
                    flagged++;
                } else if (!left[0].visible) {
                    hidden++;
                }
                amount++;
            }
        }

        //Right
        var right = null;
        if (id + 1 < board.length) {
            if (board[id + 1][0].y == board[id][0].y) {
                right = board[id + 1];
                all.push(board[id + 1]);
                if (right[0].flagged) {
                    flagged++;
                } else if (!right[0].visible) {
                    hidden++;
                }
                amount++;
            }
        }

        //Top
        var top = [];
        for (let i = id - (xLength + 1); i <= id - (xLength - 1); i++) {
            if (i >= 0) {
                if (board[i][0].y == board[id][0].y - 1) {
                    if (board[i][0].flagged) {
                        flagged++;
                    } else if (!board[i][0].visible) {
                        hidden++;
                    }
                    amount++;
                    top.push(board[i])
                    all.push(board[i])
                } else {
                    top.push(null);
                }
            } else {
                top.push(null);
            }
        }

        //bottom
        var bottom = [];
        for (let i = id + (xLength - 1); i <= id + (xLength + 1); i++) {
            if (i < board.length) {
                if (board[i][0].y == board[id][0].y + 1) {
                    if (board[i][0].flagged) {
                        flagged++;
                    } else if (!board[i][0].visible) {
                        hidden++;
                    }
                    amount++;
                    bottom.push(board[i]);
                    all.push(board[i])
                } else {
                    bottom.push(null);

                }
            } else {
                bottom.push(null);
            }
        }

        return [{
            left: left,
            right: right,
            top: top,
            bottom: bottom,
            all: all,
            amount: amount,
            hidden: hidden,
            flagged: flagged
        }];
    }
}